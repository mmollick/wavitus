﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    public bool isPlaying;

    public bool gameEnded;

    public GameObject[] targets;

    public static int buttonsPressed;

    public static float timeLeft = 60f;

    public GameObject timer;

    public GameObject arm;

    public GameObject mainScreen;

    public GameObject credits;

    public GameObject info;

    public GameObject endGamePanel;

    public bool showCredits;

    public float infoHideAfter;

    public Material activeButtonMat;

	// Use this for initialization
	void Start () {
	    displayInfo("Hit the buttons to stop Wavitus from spreading", 3.0f);
	}
	
	// Update is called once per frame
	void Update () {

	    if (Input.GetButton("Cancel"))
	    {
	        if(isActiveAndEnabled)
	            pause();
	        else
	            play();
	    }

	    if (isPlaying && !gameEnded)
	    {
	        mainScreen.SetActive(false);
	        endGamePanel.SetActive(false);
	        credits.SetActive(false);
	        timer.SetActive(true);
	        arm.SetActive(true);

	        // Update timer
	        updateTimer();

	        if (!isTargetActive())
	            selectNewTarget();

	        if (info.activeInHierarchy && infoHideAfter < Time.time)
	            info.SetActive(false);
	    }
	    else if (gameEnded)
	    {
	        endGamePanel.SetActive(true);
	        credits.SetActive(false);
	        mainScreen.SetActive(false);
	        timer.SetActive(false);
	        arm.SetActive(false);
	    }
	    else
	    {
	        if (showCredits)
	        {
	            mainScreen.SetActive(false);
	            credits.SetActive(true);
	        }
	        else
	        {
	            mainScreen.SetActive(true);
	            credits.SetActive(false);
	        }

	        endGamePanel.SetActive(false);
	        timer.SetActive(false);
	        info.SetActive(false);
	        arm.SetActive(false);
	    }

	    if (IsTimeUp())
	    {
	        endGame("Times up!");
	    }

	}

    public void play()
    {
        isPlaying = true;
        showCredits = false;
    }

    public void exit()
    {
        Application.Quit();
    }

    public void pause()
    {
        isPlaying = false;
        showCredits = false;
    }

    public void displayCredits()
    {
        isPlaying = false;
        showCredits = true;
    }

    // Select a new target
    void selectNewTarget()
    {
        int i = Random.Range(0, targets.Length - 1);
        targets[i].GetComponent<ButtonCollider>().isActive = true;
    }

    // Update UI timer
    void updateTimer()
    {
        timeLeft = timeLeft - Time.deltaTime;
        timer.GetComponentInChildren<Text>().text = string.Format("{0:0.00}", timeLeft) + " sec";
    }

    // Check if a target is active
    bool isTargetActive()
    {
        bool isActive = false;
        for (int i = 0; i < targets.Length; i++)
        {
            if (targets[i].GetComponent<ButtonCollider>().isActive)
            {
                isActive = true;
                break;
            }
        }
        return isActive;
    }

    // Check if time is up
    public static bool IsTimeUp()
    {
        return timeLeft <= 0;
    }

    public static void buttonWasPressed()
    {
        buttonsPressed++;
        if (buttonsPressed >= 10)
        {
            GameMaster gm = GameObject.Find("Main Camera").GetComponent<GameMaster>();
            gm.endGame("You stopped WAVITUS!");
        }
        else
        {
            displayInfo("Step completed!", 3);
        }
    }

    private void endGame(string message)
    {
        GameMaster gm = GameObject.Find("Main Camera").GetComponent<GameMaster>();
        gm.gameEnded = true;
        gm.endGamePanel.SetActive(true);
        gm.endGamePanel.GetComponentInChildren<Text>().text = message.ToString();
    }

    public static void displayInfo(string message, float hideAfter)
    {
        GameMaster gm = GameObject.Find("Main Camera").GetComponent<GameMaster>();
        gm.info.SetActive(true);
        gm.info.GetComponentInChildren<Text>().text = message.ToString();
        gm.infoHideAfter = Time.time + hideAfter;
    }

    public static Material getActiveButtonMat()
    {
        GameMaster gm = GameObject.Find("Main Camera").GetComponent<GameMaster>();
        return gm.activeButtonMat;
    }

}
