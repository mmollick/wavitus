﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCollider : MonoBehaviour
{
    public bool isActive;

    private Animator animator;
    private AudioSource audio;
    private Material origMat;

    private float FadeDuration = 0.5f;
    private Color startColor;
    private Color endColor;
    private float lastColorChangeTime;

    private Material material;
    private float lastPressed;

    // Use this for initialization
	void Start ()
	{
	    animator = GetComponent<Animator>();
	    audio = GetComponent<AudioSource>();
	    origMat = GetComponent<Renderer>().material;
	    startColor = Color.black;
	    endColor = new Color(255, 255, 0);
	}
	
	// Update is called once per frame
	void Update () {
	    if (isActive)
	    {
	        GetComponent<Renderer>().material = GameMaster.getActiveButtonMat();
	        var ratio = (Time.time - lastColorChangeTime) / FadeDuration;
	        ratio = Mathf.Clamp01(ratio);
	        GetComponent<Renderer>().material.color = Color.Lerp(startColor, endColor, ratio);

	        if (ratio == 1f)
	        {
	            lastColorChangeTime = Time.time;

	            // Switch colors
	            var temp = startColor;
	            startColor = endColor;
	            endColor = temp;
	        }
	    }
	    else
	    {
	        GetComponent<Renderer>().material = origMat;
	    }


	    if (lastPressed + 10f < Time.time || isActive)
	    {
	        animator.SetBool("If pressed", false);
	        animator.SetBool("Is pressed", false);
	    }

	}

    public void pressed()
    {
        if (isActive)
        {
            lastPressed = Time.time;
            isActive = false;
            GameMaster.buttonWasPressed();
            audio.enabled = true;
            audio.Play();
            animator.SetBool("If pressed", true);
            animator.SetBool("Is pressed", true);
        }
    }
}
