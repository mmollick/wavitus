﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmControl : MonoBehaviour
{

    public Transform[] joints;

    public float[] intensityPosition;
    public float[] intensityRotation;

    protected Dictionary<string, Quaternion> initialRotation = new Dictionary<string, Quaternion>();
    protected Dictionary<string, Quaternion> targetRotation = new Dictionary<string, Quaternion>();

    public float nextUpdate = 0.2f;
    private float nextTime;

    public bool isSteady;
    public float speed = 0.001f;
    public float maxSteadyLength = 5f;
    public float steadyCooldownInterval = 10f;
    public float defaultSpeed;
    public float steadyStart;

    public bool shouldSlap;
    public bool didCollide;

    // Use this for initialization
	void Start ()
	{
	    defaultSpeed = speed;

		for(int i = 0; i < joints.Length; i++)
		{
		    initialRotation.Add(joints[i].name, joints[i].localRotation);
		    targetRotation.Add(joints[i].name, joints[i].localRotation);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
        randomWave();

	    // Handle Steady
	    if (Input.GetButtonDown("Fire2")
	        && Time.time >= steadyStart + steadyCooldownInterval)
	    {
	        speed = defaultSpeed / 4;
	        isSteady = true;
	        steadyStart = Time.time;
	    }
	    else if (isSteady
	             && Time.time >= steadyStart + maxSteadyLength)
	    {
	        isSteady = false;
	        speed = defaultSpeed;
	    }

        // Handle Slap
	    if (Input.GetButtonDown("Fire1"))
	    {
	        didCollide = false;
	        shouldSlap = true;
	    }

	    if(didCollide)
	    {
	        shouldSlap = false;
	    }

	    slapSlapSlappingSounds();

	    if (Time.time >= nextTime)
	    {
	        createTargetRotation();
	        nextTime += nextUpdate;
	    }
	}

    void slapSlapSlappingSounds()
    {
        float x = (shouldSlap) ? -20f : 35.5f;
        if (transform.localRotation.eulerAngles.x != x)
        {
            transform.localRotation = Quaternion.Lerp(transform.localRotation,
                Quaternion.Euler(x, transform.localRotation.eulerAngles.y,
                    transform.localRotation.eulerAngles.z), 0.1f);
        }

//        if (transform.localRotation.eulerAngles.x < 360 && transform.localRotation.eulerAngles.x > 320)
//        {
//            shouldSlap = false;
//        }
    }

    Quaternion randomQuaternion(Quaternion root, float modifier)
    {
        float x = Random.Range(root.x - modifier , root.x + modifier);
        float y = Random.Range(root.y - modifier , root.y + modifier);
        float z = Random.Range(root.z - modifier , root.z + modifier);
        float w = root.w; //Random.Range(root.w - modifier , root.w + modifier);
        return new Quaternion(x, y, z, w);
    }

    void randomWave()
    {
        for(int i = 0; i < joints.Length; i++)
        {
            joints[i].localRotation = Quaternion.Lerp(joints[i].localRotation, targetRotation[joints[i].name], Time.time * speed);
        }
    }

    void createTargetRotation()
    {
        for(int i = 0; i < joints.Length; i++)
        {
            Quaternion rootRotation = initialRotation[joints[i].name];
            targetRotation[joints[i].name] = randomQuaternion(rootRotation, intensityRotation[i]);
        }
    }
}
