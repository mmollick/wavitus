﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float lookSpeed = 2f;
    public float dollySpeed = .5f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    float look = Input.GetAxis("Horizontal");
	    transform.localRotation = Quaternion.Euler(0,
	        Mathf.Clamp(transform.localRotation.eulerAngles.y + (look * lookSpeed), 30, 130), 0);

	    float forward = Input.GetAxis("Vertical");
	    transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x + (forward * dollySpeed), -0.25f, 0.5f),
	        transform.localPosition.y,
	        transform.localPosition.z
	    );
	}
}
