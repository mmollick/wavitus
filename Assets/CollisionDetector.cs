﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{

    public GameObject root;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ButtonCollider>())
        {
            other.gameObject.GetComponent<ButtonCollider>().pressed();
            root.GetComponent<ArmControl>().didCollide = true;
        }
        else
        {
            root.GetComponent<ArmControl>().didCollide = true;
        }
    }
}
